

//explicam pe scurt de ce nu se foloseste var
//o variabila declarata cu let isi pastreaza valoarea doar in cadrul functiei si permite o utilizare mai libera a numelor (deoarece se elibereaza pe parcurs)
//in timp ce var pastreaza variabila in intreg blocul de cod (poate cauza diverse bug-uri)

let x
x = 5
console.log(x);
const y = 3 //aici da eroare daca nu initializam
var z 


x = 5 //int
console.log(x);
console.log(typeof(x));

//y este declarat cu const, ar trb sa crape
//y = 8
console.log(y);


//aratam incrementarea
x = x + 1
x += 1

//OPERATORI

z = "5"

let adevarat = true;
let fals = false;

console.log(x > y)

console.log(x == y)
console.log(x === z) // as lasa asta pentru a explica de ce folosim 3 egaluri

console.log(x != y)

console.log(adevarat || fals)
console.log(adevarat && fals)

console.log(x == 5 && y == 3)
console.log(x == 3 || y == 3)


console.log(adevarat ? "da" : "nu")


if(x == 5)
    console.log("x este egal cu 5")
else
    console.log("x nu este egal cu 5")



//LOOP-URI
let contor = 0

//!!!DE MENTIONAT -> DACA UITI SA INCREMENTEZI CONTORUL, AJUNGI LA O BUCLA INFINITA => stack/buffer overflow

while(contor > 5){
    console.log(contor)
    contor++
}

contor = 0;

do
{
    console.log(contor);
    contor++
} while (contor < 5)

console.log(contor);

for(contor = 0; contor < 5; contor++)
{
    console.log(contor)
}


const string = "Croco a fost furat";
const stringNou = string;

console.log(string);
console.log(stringNou);

const sgl = 'Single quotes'
const dbl = "Double quotes"
console.log(sgl)
console.log(dbl)


console.log("Numarul meu este " + x);

const yourName = "Ceasca"

console.log("Numele meu are " + yourName.length + " litere") // aici pentru .length

const greeting = `Hello, ${yourName}! Ce mai faci?`
console.log(greeting)

//   DATE
let dataCurenta = Date.now()
let variabilaDeData = new Date('December 17, 1995 03:24:00')
let variabilaDeAltaData = new Date("1995-12-17T03:24:00")

console.log(dataCurenta)
console.log(variabilaDeData)
console.log(variabilaDeAltaData)

//FUNCTII

//ce face un parametru -> o variabila locala a functiei a carei valoare se seteaza la apelul functiei
function afisareNume(nume)
{
    console.log("Hello, " + nume)
}

afisareNume("Mihai")


//de prezentat ca orice e dupa return nu se ia in calcul
function suma(a,b)
{
    let rezultat
    rezultat = a + b
    return rezultat
}

//transformam functia sa fie rezolvata doar din return
let rez = suma(10,30);
console.log(rez);
console.log(suma(10,30))


//arrow functions
const sumaArrow = (a,b) => {
    return a + b
};

console.log(sumaArrow(3,4))



//ARRAYS


const arr = []


const nume = ['Leo', 'Margi', 'Ceasca', 'Visan', 'Gabi']

console.log(nume[0])

console.log(nume.length);

nume.push("Cristi")
console.log(nume[5])

nume.pop()
console.log(nume[5])

const filter = nume.filter((word) => word.length > 4); // returneaza un shallow copy doar cu elementele care indeplinesc conditia data
console.log(filter);

const find = nume.find((x) => x.length > 4); // returneaza primul element care indeplineste conditia data
console.log(find);

const map = nume.map((x) => x = x.length); // creaza un nou array dupa conditia data
console.log(map);

console.log(nume.slice(2)); // returneaza un shallow copy intre start si end, aici am pus doar start

console.log(nume.slice(2, 4)); // aici este de la pozitia 2 pana la 4

nume.sort(); // rearanjeaza array ul initial
console.log(nume);

nume.forEach((element) => console.log(element)); // executa comanda data pentru fiecare element din array

for(let i = 0; i < nume.length; i++)
{
    console.log(nume[i]);
}

for(const element of nume){
    console.log(element);
} // fiecare iteratie a for-ului este reprezentata de un element gasit in array-ul nume


//OBIECTE

let obiect = {}

let masina = {
    marca: "CrocoMobil",
    culoare: "Verde",
    anFabricatie: 1996,
    asigurata: true,
    viteza: 10,

    acceleratie: function()
    {
        this.viteza = this.viteza + 10;
    },
}

console.log(masina.marca)

//aratam ca putem sa modificam
masina.asigurata = false
console.log(masina.asigurata)

//aratam ca putem sa adaugam proprietati din cod
masina.numeProprietar = "Liviu"

//adaugi in obiect viteza si functiile
//spunem ca functiile din cadrul obiectelor se numesc metode
console.log(masina.viteza)

masina.acceleratie()
console.log(masina.viteza)

console.log(Object.keys(masina)); // returneaza lista de proprietati/chei ale obiectului

console.log(Object.values(masina)); // returneaza lista de valori ale proprietatilor din obiect

console.log(Object.entries(masina)); // returneaza un array cu perechile proprietati/chei - valoare

console.log("aaaaaaaaaaaaaaaaaaaaaaaaa")

for (const property in masina) {
    console.log(`${property}: ${masina[property]}`);
} // fiecare iteratie/pas al for-ului este reprezentata de o proprietate gasita in obiect



//REGEX
//explicam: provine de la REGular EXpression si 
//este o metoda ce ne ajuta sa cautam tipare in stringuri

//sintaxa
//^=start of the line
//[]=aici introducem tiparul pe care il cautam
//pot fi litere, numere, underscore
//aratam cum dam escape la cratima ca si caracter special \\
//aratam cum indicam ca vrem sa caute un spatiu (\s) 
//(optional) {}=dimensiunea stringului pe care il cautam
//$=end of the line


if(nume[0].match("^[0-9]$")){
    console.log("Numele introdus contine cifre")
} else {
    console.log("Ai introdus numele cu succes")
}

//le putem arata toolul online de a verifica regex
//regexr.com
//regex101.com

// PROMISE

const greaterThanTen = (number) => {
    return new Promise((resolve, reject) => {
        if (number > 10) setTimeout(() => resolve(`the number ${number} is greater than 10! :\)`), 1000);
        else setTimeout(() => reject(`the number ${number} is smaller than 10, womp wom :c`), 1000);
    });
}

greaterThanTen(12)
    .then(response => console.log({type: 'success', message: response}))
    .catch(error => console.log({type: 'error', message: error}));
    
console.log('done!');    
    
const main = async () => {
    try {
        const response = await greaterThanTen(12);
        console.log({type: 'success', message: response});
    } catch (error) {
        console.log({type: 'error', message: error});
    }
}


main();